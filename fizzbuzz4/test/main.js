'use strict';
const expect = require('chai').expect;

const lookupTable = [
  'fizzbuzz',
  1,
  2,
  'fizz',
  4,
  'buzz',
  'fizz',
  7,
  8,
  'fizz',
  'buzz',
  11,
  'fizz',
  13,
  14,
  'fizzbuzz',
];

function fizzbuzz(num) {
  return lookupTable[num % 15];
}

describe('fizzbuzz(1)', () => {
  it('should return 1', () => {
    expect(fizzbuzz(1)).to.eql(1);
  });
});

describe('fizzbuzz(2)', () => {
  it('should return 2', () => {
    expect(fizzbuzz(2)).to.eql(2);
  });
});

describe('fizzbuzz(3)', () => {
  it('should return 3', () => {
    expect(fizzbuzz(3)).to.eql('fizz');
  });
});

describe('fizzbuzz(6)', () => {
  it('should return 6', () => {
    expect(fizzbuzz(6)).to.eql('fizz');
  });
});

describe('fizzbuzz(5)', () => {
  it('should return 5', () => {
    expect(fizzbuzz(5)).to.eql('buzz');
  });
});

describe('fizzbuzz(10)', () => {
  it('should return 10', () => {
    expect(fizzbuzz(10)).to.eql('buzz');
  });
});

describe('fizzbuzz(15)', () => {
  it('should return 15', () => {
    expect(fizzbuzz(15)).to.eql('fizzbuzz');
  });
});

describe('fizzbuzz(30)', () => {
  it('should return 30', () => {
    expect(fizzbuzz(30)).to.eql('fizzbuzz');
  });
});
