'use strict';
const expect = require('chai').expect;

class FizzAndOrBuzz {}

class Integer {
  toStringOfFizzAndOrBuzz(integerToBeTransformed) {
    if (integerToBeTransformed % 15 == 0) {
      return 'fizzbuzz';
    }
    if (integerToBeTransformed % 3 == 0) {
      return 'fizz';
    }
    if (integerToBeTransformed % 5 == 0) {
      return 'buzz';
    }

    return '' + integerToBeTransformed;
  }
}


function integerToStringOfFizzAndOrBuzzOrItself(integerToBeTransformed) {
  if (integerToBeTransformed % 15 == 0) {
    return 'fizzbuzz';
  }
  if (integerToBeTransformed % 3 == 0) {
    return 'fizz';
  }
  if (integerToBeTransformed % 5 == 0) {
    return 'buzz';
  }

  return '' + integerToBeTransformed;
}

describe('transformIntegerToStringOfFizzAndOrBuzzOrItself(1)', () => {
  it('should return 1', () => {
    expect(transformIntegerToStringOfFizzAndOrBuzzOrItself(1)).to.eql('1');
  });
});

describe('transformIntegerToStringOfFizzAndOrBuzzOrItself(3)', () => {
  it('should return fizz', () => {
    expect(transformIntegerToStringOfFizzAndOrBuzzOrItself(3)).to.eql('fizz');
  });
});

describe('transformIntegerToStringOfFizzAndOrBuzzOrItself(5)', () => {
  it('should return buzz', () => {
    expect(transformIntegerToStringOfFizzAndOrBuzzOrItself(5)).to.eql('buzz');
  });
});

describe('transformIntegerToStringOfFizzAndOrBuzzOrItself(15)', () => {
  it('should return fizzbuzz', () => {
    expect(transformIntegerToStringOfFizzAndOrBuzzOrItself(15))
        .to.eql('fizzbuzz');
  });
});
