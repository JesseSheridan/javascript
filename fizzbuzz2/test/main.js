'use strict';
const expect = require('chai').expect;

function fizzbuzz(number) {
  if (dividedBy(number, 15)) {
    return 'fizzbuzz';
  }
  if (dividedBy(number, 3)) {
    return 'fizz';
  }
  if (dividedBy(number, 5)) {
    return 'buzz';
  }

  return number;
}

function dividedBy(number, divisor) {
  return number % divisor == 0;
}


describe('fizzbuzz(1)', () => {
  it('should return 1', () => {
    expect(fizzbuzz(1)).to.eql(1);
  });
});

describe('fizzbuzz(3)', () => {
  it('should return fizz', () => {
    expect(fizzbuzz(3)).to.eql('fizz');
  });
});

describe('fizzbuzz(6)', () => {
  it('should return fizz', () => {
    expect(fizzbuzz(6)).to.eql('fizz');
  });
});

describe('fizzbuzz(5)', () => {
  it('should return buzz', () => {
    expect(fizzbuzz(5)).to.eql('buzz');
  });
});

describe('fizzbuzz(15)', () => {
  it('should return fizzbuzz', () => {
    expect(fizzbuzz(15)).to.eql('fizzbuzz');
  });
});

describe('fizzbuzz(30)', () => {
  it('should return fizzbuzz', () => {
    expect(fizzbuzz(30)).to.eql('fizzbuzz');
  });
});
