'use strict';
const expect = require('chai').expect;

describe('GameOfLife', function() {
    it('should return 42', function() {
        expect(life()).to.eql(42);
    });
});

function life() {
    return 1;
}
