module.exports = function(config) {
  config.set({
    testRunner: 'mocha',
    mutator: 'javascript',
    transpilers: [],
    reporter: ['clear-text', 'progress', 'html'],
    packageManager: 'npm',
    testFramework: 'mocha',
    coverageAnalysis: 'perTest',
    mutate: ['./**/*.js'],
  });
};
