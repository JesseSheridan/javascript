module.exports = {
    'extends': ['eslint:recommended', 'google'],
    'rules': {
        'valid-jsdoc': 0,
        'require-jsdoc': 0,
        'indent': 0,
    },
    'env': {
        'browser': true,
        'node': true,
        'webextensions': true,
        'jquery': true,
        'mocha': true,
    },
    'parserOptions': {
        'ecmaVersion': 6
    }
};
