'use strict';
const expect = require('chai').expect;

function fib(num) {
  if (num == 0 || num == 1) {
    return 1;
  }
  return fib(num - 1) + fib(num - 2);
}

describe('fib(0)', () => {
  it('should return 1', () => {
    expect(fib(0)).to.eql(1);
  });
});

describe('fib(1)', () => {
  it('should return 1', () => {
    expect(fib(1)).to.eql(1);
  });
});

describe('fib(2)', () => {
  it('should return 2', () => {
    expect(fib(2)).to.eql(2);
  });
});

describe('fib(3)', () => {
  it('should return 3', () => {
    expect(fib(3)).to.eql(3);
  });
});

describe('fib(4)', () => {
  it('should return 5', () => {
    expect(fib(4)).to.eql(5);
  });
});

describe('fib(5)', () => {
  it('should return 8', () => {
    expect(fib(5)).to.eql(8);
  });
});
